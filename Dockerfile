ARG DISTRO=debian
ARG CODENAME=buster
FROM $DISTRO:$CODENAME

RUN set -ex; \
	apt-get update --quiet; \
	DEBIAN_FRONTEND=noninteractive apt-get install --quiet --no-install-recommends --yes \
		netcat \
		openssh-server \
	; \
	sed -i -e 's,^#\?HostKey\s\+/etc/ssh/\(.*\)$,HostKey /etc/ssh/keys/\1,' \
		/etc/ssh/sshd_config; \
	mkdir -p /etc/ssh/keys; \
	rm -rf /var/lib/apt/lists/*; \
	mkdir -p /var/run/sshd

HEALTHCHECK --start-period=30s --interval=5m --timeout=3s \
	CMD /usr/bin/nc -z localhost 22 || exit 1

VOLUME ["/etc/ssh/keys"]
EXPOSE 22
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
