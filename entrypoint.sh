#!/bin/sh

set -ex

HOST_KEYS_DIR=/etc/ssh/keys

# Generate Host keys, if required
if ls "${HOST_KEYS_DIR}"/ssh_host_* 1>/dev/null 2>&1; then
  echo ">> Host keys in keys directory"
else
  echo ">> Generating new host keys"
  ssh-keygen -A
  mv /etc/ssh/ssh_host_* "${HOST_KEYS_DIR}"
fi

# print fingerprints
for item in dsa rsa ecdsa ed25519; do
  echo ">>> Fingerprints for ${item} host key"
  ssh-keygen -E md5 -lf "${HOST_KEYS_DIR}/ssh_host_${item}_key"
  ssh-keygen -E sha256 -lf "${HOST_KEYS_DIR}/ssh_host_${item}_key"
  ssh-keygen -E sha512 -lf "${HOST_KEYS_DIR}/ssh_host_${item}_key"
done

exec /usr/sbin/sshd -D -e
